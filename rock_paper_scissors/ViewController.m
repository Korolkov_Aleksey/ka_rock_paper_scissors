//
//  ViewController.m
//  rock_paper_scissors
//
//  Created by Korolkov Aleksey on 19.02.16.
//  Copyright © 2016 Korolkov Aleksey. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

@end
