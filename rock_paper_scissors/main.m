//
//  main.m
//  rock_paper_scissors
//
//  Created by Korolkov Aleksey on 19.02.16.
//  Copyright © 2016 Korolkov Aleksey. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
