//
//  AppDelegate.h
//  rock_paper_scissors
//
//  Created by Korolkov Aleksey on 19.02.16.
//  Copyright © 2016 Korolkov Aleksey. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

